import {HttpTransportType, HubConnection, HubConnectionBuilder, LogLevel} from "@microsoft/signalr";
import React, {useEffect, useState} from 'react';
import { Button } from "@mui/material";

const Customer = () => {
    const [connection, setConnection] = useState<null | HubConnection>(null) ;
    const [getCustomersAsyncResult, setCustomersResult] = useState("");

    const [customerGuid, setCustomerGuid] = useState("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
    const [getCustomerAsyncResult, setCustomerResult] = useState("");

    const cust = {
        id: "06c8c6b1-4349-45b0-ab31-244740aaf0f0",
        firstName: "Иванов",
        lastName: "Иван",
        email: "temp@mail.ru",
        preferences: [
            {
              "id": "76324c47-68d2-472d-abb8-33cfa8cc0c84",
              "name": "Дети"
            },
            {
              "id": "ef7f299f-92d7-459f-896e-078ed53ef99c",
              "name": "Театр"
            }
          ],
        promoCodes: []
      };

    const [customerValue, setCustomer] = useState(cust);
    const [createCustomerAsyncResult, setCreateCustomerResult] = useState("");

    
    useEffect(() => {
        console.log('start');

        const connect = new HubConnectionBuilder()
            .configureLogging(LogLevel.Debug)
            .withUrl("https://localhost:5001/hubs/customer",{
                skipNegotiation: true,
                transport: HttpTransportType.WebSockets
            })
            .withAutomaticReconnect()
            .build();
        setConnection(connect);
    }, []);

    useEffect(() => {
        if (connection)
        {
            connection
            .start()
            .then(() => {
                console.log('connect');
                connection.on("GetCustomersAsync", handleReceiveMessage);
            })
            .catch((error) => console.log(error));
        }
    }, [connection]);

    const handleReceiveMessage = (responseMessage) =>{
        console.log(JSON.stringify(responseMessage));
    }

    const GetCustomers = async () => {
        console.log("GetCustomers");
        if (connection){
          const a = await connection.invoke("GetCustomersAsync");
          console.log(JSON.stringify(a));
          setCustomersResult(JSON.stringify(a));
        }
      };
    
      const GetCustomer = async () => {
        console.log("GetCustomer");
        if (connection){
          const a = await connection.invoke("GetCustomerAsync", customerGuid );
          console.log(JSON.stringify(a));
          setCustomerResult(JSON.stringify(a));
        }
      };

      const CreateCustomer = async () => {
        console.log("CreateCustomer");
        if (connection){
          const a = await connection.invoke("CreateCustomerAsync", customerValue );
          console.log(JSON.stringify(a));
          setCreateCustomerResult(JSON.stringify(a));
        }
      };
    
    return (
        <div style={{textAlign: "left"}}>
            Почти swagger: 
            <div><button onClick={GetCustomers} >GetCustomersAsync</button><label>{getCustomersAsyncResult}</label></div>
            <hr/>
            <div>
                <button onClick={GetCustomer} >GetCustomerAsync</button>
                <input value={customerGuid} onChange={(input) => { setCustomerGuid(input.target.value); }} />
            </div>
            <div>
                <label>{getCustomerAsyncResult}</label>
            </div>
            <hr/>
            <div>
                <button onClick={CreateCustomer} >GetCustomerAsync</button>
                <textarea value ={JSON.stringify(customerValue)} rows={6} readOnly  style={{width: "500px"}} />
            </div>
            <div>
                <label>{createCustomerAsyncResult}</label>
            </div>
            <hr/>
        </div>
    );

}
export default Customer;